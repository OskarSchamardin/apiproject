/******************************/
/* SETUP VARIABLES AND USINGS */
/******************************/

/* requirements */
const express = require("express");
const mysql = require("mysql");
const jwt = require("jsonwebtoken");

/* important variables */
const app = express();
const port = process.env.PORT || 3000;
const url = "/api";
/* Connect node and mysql: https://invidio.us/watch?v=XuLRKMqozwA */
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'APIProjectDB',
    insecureAuth: true
});

connection.connect();

app.use(express.json());

/* enable CORS */
app.use(function(req, res, next)
{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

EnsureThatTableAccountsExists();
EnsureThatTableTasksExists();

/****************/
/* END OF SETUP */
/****************/

let tokenOfClient;
let usernameOfClient;
let useridOfClient;

/* send example json to webpage */
app.get(url, function(req, res)
{
    console.log("\nReceived GET request: Request for EXAMPLE DATA, sending example json...")
    res.statusMessage = "Sending example json...";
    res.status(200).json(["Foo", "Bar", "Baz", "Qux", "FooBar"]).end();
});

/* Register */
app.post(`${url}/register`, function(req, res)
{
    console.log("\nReceived POST request: Request to REGISTER, proceeding with registration...")
    let post = req.body;
    RegisterAccount(post, function(response, status)
    {
        /* https://stackoverflow.com/questions/14154337/how-to-send-a-custom-http-status-message-in-node-express#36507614 */
        res.statusMessage = response;
        res.status(status).end();
    });
});

/* Login */
app.post(`${url}/login`, function(req, res)
{
    console.log("\nReceived POST request: Request to LOG IN, checking login credentials...")
    let post = req.body;
    ValidateAccount(post, function(response, status, token, username, userid)
    {
        if(token != null)
        {
            tokenOfClient = token;
            usernameOfClient = username;
            useridOfClient = userid;
            res.statusMessage = response;
            res.status(status).json(token).end();
        }
        else
        {
            res.statusMessage = response;
            res.status(status).end();
        }
    });
});

/* Verify That User Is Logged In */
app.post(`${url}/verify`, function(req, res)
{
    console.log("\nReceived POST request: Request to VERIFY THAT USER IS LOGGED IN, checking login credentials...")
    let post = req.body;
    if(post.token === tokenOfClient)
    {
        console.log("USER IS VERIFIED AND AUTHORIZED.");
        res.statusMessage = "Token is valid.";
        res.status(200).end();
    }
    else
    {
        console.log("USER IS UNAUTHORIZED.");
        res.statusMessage = "Token is invalid.";
        res.status(403).end();
    }
});

/* Logout */
app.get(`${url}/logout`, function(req, res)
{
    console.log("\nReceived GET request: Request to LOG OUT, removing session...")
    tokenOfClient = null;
    usernameOfClient = null;
    useridOfClient = null;
    res.statusMessage = "You have ended the session, token has been removed and user has been logged out.";
    res.status(204).end();
});

/* Load Tasks */
app.post(`${url}/load`, VerifyToken, function(req, res)
{
    console.log("\nReceived POST request: Request to LOAD TASKS, verifying login...");
    jwt.verify(req.body.token, 'secretkey', { expiresIn: '1d'}, function(err, authData)
    {
        if(err)
        {
            console.log("USER IS UNAUTHORIZED TO LOAD TASKS.");
            res.statusMessage = "Token is invalid.";
            res.status(403).end();
        }
        else
        {
            console.log("USER IS VERIFIED AND AUTHORIZED TO LOAD TASKS.");
            console.log(`\nFETCHING TASKS FOR USER \"${usernameOfClient}\" FROM DATABASE...`);
            GetTasks(usernameOfClient, function(dbResult)
            {
                console.log(`TASKS FETCHED AND BEING SENT TO USER \"${usernameOfClient}\".`);
                res.statusMessage = "Token is valid, sending tasks from database...";
                res.status(200).json(dbResult);
            });
        }
    });
});

/* Save Tasks */
app.post(`${url}/save`, VerifyToken, function(req, res)
{
    console.log("\nReceived POST request: Request to SAVE TASKS, verifying login...");
    jwt.verify(req.body.token, 'secretkey', { expiresIn: '1d'}, function(err, authData)
    {
        if(err)
        {
            console.log("USER IS UNAUTHORIZED TO SAVE TASKS.");
            res.statusMessage = "Token is invalid.";
            res.status(403).end();
        }
        else
        {
            console.log("USER IS VERIFIED AND AUTHORIZED TO SAVE TASKS.");
            console.log(`\nSAVING TASKS FOR USER \"${usernameOfClient}\" TO DATABASE.`);
            if(typeof req.body.tasks !== 'undefined' && req.body.tasks)
            {
                SaveTasks(req.body.tasks, function(response, status)
                {
                    res.statusMessage = response;
                    res.status(201).end();
                });
            }
            else
            {
                res.statusMessage = "Token is valid, cannot save tasks to database, tasks is undefined.";
                res.status(400).end();
            }
        }
    });
});

/* listen for requests */
app.listen(port, () =>
{
    console.log(`Server running on port: ${port}`);
    console.log(`Access API at: \"localhost:${port}${url}\"`);
});


/*************/
/* FUNCTIONS */
/*************/

function ValidateAccount(post, callback)
{
    /* Callback functions: https://invidio.us/watch?v=haz4SBcEYAw */
    /* https://stackoverflow.com/questions/5010288/how-to-make-a-function-wait-until-a-callback-has-been-called-using-node-js#5010339 */
    if(typeof post.user !== 'undefined' && post.user && typeof post.pass !== 'undefined' && post.pass) 
    {
        GetAccount(post.user, function(dbResult)
        {
            try
            {
                if (post.user === dbResult.username && post.pass === dbResult.password)
                {
                    console.log("AUTHENTICATION SUCCESS.");
                    jwt.sign({dbResult}, 'secretkey', function(err, token)
                    {
                        if(err)
                        {
                            console.log(err);
                            callback(err, 500);
                        }

                        callback(`You Are Now Logged In.`, 200, token, dbResult.username, dbResult.id);
                    });
                }
                else
                {
                    console.log("AUTHENTICATION FAILURE.");
                    callback("Bad Username/Password", 403);
                }
            }
            catch(err)
            {
                console.log("AUTHENTICATION FAILURE.");
                console.error(err);
                callback("Bad Username/Password", 403);
            }
        });
    }
    else
    {
        console.log("AUTHENTICATION FAILURE.");
        callback("Bad Username/Password", 403);
    }
}

function GetAccount(username, callback)
{
    /* Used to get account data and validate it */
    connection.query('SELECT * FROM accounts WHERE username=?', username, function(err, result){
        if(err)
        {
            console.error(err);
            return(err);
        }
        callback(result[0]);
    });
}

function GetTasks(username, callback)
{
    /* Used to get account data and validate it */
    connection.query('SELECT title, description, finished FROM tasks, accounts WHERE accounts.username = ? AND userid = accounts.id', username, function(err, result){
        if(err)
        {
            console.error(err);
            return(err);
        }
        callback(result);
    });
}

function SaveTasks(task, callback)
{
    connection.query('DELETE FROM tasks WHERE userid = ?', useridOfClient, function(err, result){
        if(err)
        {
            console.error(err);
            callback(err, 500);
        }
    });

    /* parse array */
    for(let i = 0; i < task.length; ++i)
    {
        let taskTitle = task[i][0];
        let taskDescription = task[i][1];
        let taskFinishStatus;

        if(task[i][2])
        {
            taskFinishStatus = 1;
        }
        else
        {
            taskFinishStatus = 0;
        }

        /* add task */
        let newTask = { userid: useridOfClient, title: taskTitle, description: taskDescription, finished: taskFinishStatus };
        connection.query('INSERT INTO tasks set ?', newTask, function(err, result){
            if(err)
            {
                console.error(err);
                callback(err, 500);
            }
        });
    }
    callback("Tasks Saved.", 201);
}

function RegisterAccount(post, callback)
{
    if(typeof post.user !== 'undefined' && post.user && typeof post.pass !== 'undefined' && post.pass) 
    {
        NewAccount(post.user, post.pass, function(response, status)
        {
            callback(response, status);
        });
    }
    else
    {
        console.error(`Received invalid registration syntax, stopping...`);
        console.error(`Username: ${post.user}`);
        console.error(`Password: ${post.pass}`);
        callback(`Invalid Registration Syntax.`, 403);
    }
}

function NewAccount(username, password, callback)
{
    GetAccount(username, function(dbResult)
    {
        if(!dbResult)
        {
            let account = { username: username, password: password };
            connection.query('INSERT INTO accounts set ?', account, function(err, result){
                if(err)
                {
                    console.error(err);
                    callback(err, 500);
                }
                console.log("CREATING ACCOUNT AND INSERTING DATA INTO TABLE \"accounts\"...");
                console.log(result);
                callback(`Account Created.`, 201);
            });
        }
        else
        {
            console.error(`ACCOUNT WITH NAME \"${username}\" ALREADY EXISTS, ABORTING REGISTRATION...`)
            callback(`Username Is Taken.`, 403);
        }
    });
}

function EnsureThatTableAccountsExists()
{
    /* make sure that table: "accounts" exists */
    connection.query('CREATE TABLE IF NOT EXISTS accounts (id INT PRIMARY KEY AUTO_INCREMENT, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL);', function(err, result){
        if(err)
        {
            console.error(err);
            return;
        }
        console.log("\nGENERATING TABLE \"accounts\"");
        AddExampleAccountToAccountsTable();
        console.error(result);
    });
}

function AddExampleAccountToAccountsTable()
{
    /* add example account */
    let account = { id: 1, username:"foo", password:"bar" };
    connection.query('INSERT IGNORE INTO accounts set ?', account, function(err, result){
        if(err)
        {
            console.error(err);
            return;
        }
        console.log("\nGENERATING EXAMPLE DATA FOR TABLE \"accounts\"...");
        console.error(result);
    });
}

function EnsureThatTableTasksExists()
{
    /* make sure that table: "tasks" exists */
    connection.query('CREATE TABLE IF NOT EXISTS tasks (id INT PRIMARY KEY AUTO_INCREMENT, userid INT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, finished BOOL, CONSTRAINT fk_userid FOREIGN KEY (userid) REFERENCES accounts(id));', function(err, result){
        if(err)
        {
            console.error(err);
            return;
        }
        console.log("\nGENERATING TABLE \"tasks\"");
        AddExampleTaskToTasksTable();
        console.error(result);
    });
}

function AddExampleTaskToTasksTable()
{
    /* add example task */
    let task = { id: 1, userid: 1, title:"foobar", description:"baz", finished: 1 };
    connection.query('INSERT IGNORE INTO tasks set ?', task, function(err, result){
        if(err)
        {
            console.error(err);
            return;
        }
        console.log("\nGENERATING EXAMPLE DATA FOR TABLE \"tasks\"...");
        console.error(result);
    });
}

function VerifyToken(req, res, next)
{
    /* https://invidio.us/watch?v=7nafaH9SddU */

    const bearerHeader = req.body.token;
    if(typeof bearerHeader !== 'undefined')
    {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
    else
    {
        res.sendStatus(403);
    }
}
