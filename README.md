# APIProject

This is a simple REST API made in node.js with the help of: Express.js, MySQL.js and JsonWebToken.

This project is **NOT** intended to be used on a live environment.

## Features

* sending of example json.


* registering accounts.


* logging in/out.


* use of login tokens.


* verification of user.


* saving/loading of tasks.


## How To Use This API

This API has been tested with [Insomnia](https://github.com/Kong/insomnia). I suggest using it for best results.

### Using The API

* Clone this repository.
    ```bash
    git clone https://gitlab.com/OskarSchamardin/apiproject
    cd apiproject
    ```

* Ensure that a MySQL database exists on the host machine.
    ```sh
    sudo mysql
    CREATE DATABASE APIProjectDB;
    exit
    ```
* install dependencies and run.
    ```bash
    npm i
    npm run start
    ```
### Sending Requests

By default requests should be sent to "localhost:\<PORT\>/api" (assuming PORT environment variable is defined, otherwise it will use 3000).

#### GET Requests

There are two urls that will accept **GET requests**:

##### /api

* "\<URL:PORT\>/api" Will return example JSON with status 200.

##### /api/logout

* "\<URL:PORT\>/api/logout" Will delete the login session and return status 204.

#### POST Requests

There are five urls that will accept **POST requests**:

##### /api/register

* "\<URL:PORT\>/api/register" Expects that the request body contains json with non-empty strings 'user' and 'pass'. A successful request will create a new account in the database with corresponding details and return status 201, otherwise return status 403.
    example POST request:
    ```json
    {
        "user":"bar",
        "pass":"baz"
    }
    ```
    ...will create an account with the name "bar" and password "baz", assuming an account with the same name does not exist.

##### /api/login

* "\<URL:PORT\>/api/login" Expects that the request body contains json with non-empty strings 'user' and 'pass'. A successful request will return a session token and status 200, otherwise return status 403.
    example POST request:
    ```json
    {
        "user":"bar",
        "pass":"baz"
    }
    ```
    ...will return a generated session token, assuming an account with the same name exists and the password is correct.
    example response:
    ```json
    {
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYlJlc3VsdCI6eyJpZCI6MywidXNlcm5hbWUiOiJiYXIiLCJwYXNzd29yZCI6ImJheiJ9LCJpYXQiOjE1ODgyNTkzODJ9.fMOxa53uIKc31TACbWEOiLCKZopefSbk1k7kPsMN5r8"
    }
    ```
    This response can be used for the last 3 post requests.

##### /api/verify

* "\<URL:PORT\>/api/verify" Expects that the request body contains json with non-empty string 'token'. A successful request will return status 200, otherwise return status 403.
    example POST request:
    ```json
    {
        "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYlJlc3VsdCI6eyJpZCI6MywidXNlcm5hbWUiOiJiYXIiLCJwYXNzd29yZCI6ImJheiJ9LCJpYXQiOjE1ODgyNTkzODJ9.fMOxa53uIKc31TACbWEOiLCKZopefSbk1k7kPsMN5r8"
    }
    ```
    ...will return status 200, indicating that the token is valid, otherwise return status 403, indicating that the token was not accepted and user is considered to be unauthorized to perform restricted actions.

##### /api/load

* "\<URL:PORT\>/api/load" Expects that the request body contains json with non-empty string 'token'. A successful request will return a json array and status 200, otherwise return status 403.
    example POST request:
    ```json
    {
        "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYlJlc3VsdCI6eyJpZCI6MywidXNlcm5hbWUiOiJiYXIiLCJwYXNzd29yZCI6ImJheiJ9LCJpYXQiOjE1ODgyNTkzODJ9.fMOxa53uIKc31TACbWEOiLCKZopefSbk1k7kPsMN5r8"
    }
    ```
    ...will return a json array of tasks the user has stored in the database (if empty, will return empty array) and status 200, otherwise return status 403, indicating that the token was not accepted and user is considered to be unauthorized to load tasks.
    example response:
    ```json
    [
        {
            "title": "foo",
            "description": "bar",
            "finished": 1
        },
        {
            "title": "baz",
            "description": "qux",
            "finished": 0
        }
    ]
    ```

##### /api/save

* "\<URL:PORT\>/api/save" Expects that the request body contains a session token and a 2d json array with the first two elements being non-empty strings and the third element being a boolean value. A successful request will replace All tasks in the database corresponding to the provided info, otherwise return status 403.
    example POST request:
    ```json
    {
        "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYlJlc3VsdCI6eyJpZCI6MywidXNlcm5hbWUiOiJiYXIiLCJwYXNzd29yZCI6ImJheiJ9LCJpYXQiOjE1ODgyNTkzODJ9.fMOxa53uIKc31TACbWEOiLCKZopefSbk1k7kPsMN5r8",
        "tasks":[["foo", "bar", true], ["baz", "qux", false]]
    }
    ```
    ...will return status 201, indicating that the posted tasks have been saved to the database, otherwise return status 403.
